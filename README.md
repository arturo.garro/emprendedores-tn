# Emprendedores TN

Sitio de emprendedores de Terrazas del Norte

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/arturo.garro/emprendedores-tn.git
git branch -M main
git push -uf origin main
```

***

## Name
Sitio de emprendedores de Terrazas del Norte

## Description
Este sitio web está pensado y diseñado para ofrecer un catálogo de productos y servicios dentro del condominio

## Installation
To do

## Usage
To Do

## Support
Para soporte, a arturo.garro@asicomolooye.com


## Contributing


## Authors and acknowledgment
Arturo Garro

## License
For open source projects, say how it is licensed.

## Project status
Aún en desarrollo
